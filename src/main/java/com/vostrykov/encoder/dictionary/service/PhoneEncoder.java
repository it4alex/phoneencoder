package com.vostrykov.encoder.dictionary.service;

import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.vostrykov.encoder.dictionary.Validator;
import com.vostrykov.encoder.dictionary.model.Chain;
import com.vostrykov.encoder.dictionary.model.Word;

import java.util.*;

/**
 * Created by alex on 7/12/15.
 */
public class PhoneEncoder implements IEncoder<String> {
    @Inject
    IDictionary<String, Set<Word>> dictionary;
    Joiner joinOnEmpty = Joiner.on("").skipNulls();

    public PhoneEncoder() {
    }

    @Override
    public List<String> find(String phone) {
        return findMatchedWords(phone).getElements();
    }

    @Override
    public List<String> format(String phone, List<String> words) {
        List<String> formattedWords = Lists.newArrayList();
        for (String word : words) {
            String line = String.format("%s: %s", phone, word);
            formattedWords.add(line);
        }
        return formattedWords;
    }

    public Chain findMatchedWords(String phone) {
        Preconditions.checkArgument(Validator.isCorrectInputPhone(phone),
                String.format("Phone [%s] is not correct.", phone));

        phone = Validator.cleanPhone(phone);
        Map<Integer, Chain> combinations = findCombinations(phone);
        Map<String, Set<Word>> partVariants = getParts(phone, combinations);

        combinations = filterCombinations(combinations, partVariants.keySet());

        List<Chain> chains = findChains(combinations, phone);
        List<Chain> filteredChains = filterChains(partVariants, chains);
        Chain matchedWords = findMatchedWords(partVariants, filteredChains);

        return filterMatchedWords(matchedWords);

    }

    private Chain filterMatchedWords(Chain matchedWordChains) {
        Chain filtered = new Chain();
        for (String chain : matchedWordChains.getElements()) {
            if (!isContainsWordAnalog(chain, matchedWordChains) &&
                    !isContainsOnlyNumber(chain)) {
                filtered.getElements().add(chain);
            }

        }
        return filtered;
    }

    private boolean isContainsOnlyNumber(String chain) {
        return chain.matches("\\d+");
    }

    protected boolean isContainsWordAnalog(String word, Chain chains) {
        for (int index = 0; index < word.length(); index++) {
            if (Character.isDigit(word.charAt(index))) {
                for (String chain : chains.getElements()) {
                    if (!word.equals(chain)) {
                        if (isWordWithFirstLetterExists(index, chain) ||
                                isWordWithFirstLetterAndSpaceBeforeExists(index, chain)) return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isWordWithFirstLetterAndSpaceBeforeExists(int index, String chain) {
        return chain.length() > index && !Character.isDigit(chain.charAt(index)) && chain.charAt(index - 1) == ' ';
    }

    private boolean isWordWithFirstLetterExists(int index, String chain) {
        if (index == 0) {
            if (chain.length() > index && !Character.isDigit(chain.charAt(index))) return true;
        }
        return false;
    }

    protected Map<Integer, Chain> filterCombinations(Map<Integer, Chain> combinations, Set<String> parts) {
        for (Integer key : combinations.keySet()) {
            Chain values = combinations.get(key);
            Iterator<String> iterator = values.getElements().iterator();
            while (iterator.hasNext()) {
                String value = iterator.next();
                if (!parts.contains(value)) {
                    iterator.remove();
                }
            }
        }
        return combinations;
    }

    private Map<String, Set<Word>> getParts(String phone, Map<Integer, Chain> combinations) {
        Map<String, Set<Word>> partVariants = Maps.newLinkedHashMap();
        for (int i = 0; i < phone.length(); i++) {
            Chain phoneParts = combinations.get(i);
            for (String phonePart : phoneParts.getElements()) {
                Set<Word> words = dictionary.read().get(phonePart);
                if (words != null) {
                    addVariants(partVariants, phonePart, words);

                } else {
                    if (phonePart.length() == 1) {
                        Set<Word> singleSet = Sets.newHashSet(new Word(phonePart));
                        addVariants(partVariants, phonePart, singleSet);
                    }
                }
            }
        }
        return partVariants;
    }


    protected List<Chain> findChains(Map<Integer, Chain> combinations, final String phone) {
        List<Chain> result = Lists.newArrayList();
        for (Integer index : combinations.keySet()) {
            Chain indexedCombinations = combinations.get(index);
            for (String part : indexedCombinations.getElements()) {
                if (index == 0) {
                    result.add(new Chain(part));
                } else {
                    Iterator<Chain> resultIterator = result.iterator();
                    List<Chain> clonedChains = null;
                    while (resultIterator.hasNext()) {
                        Chain parts = resultIterator.next();
                        if (listToString(parts).length() > index) {
                            if (clonedChains == null) clonedChains = Lists.newArrayList();
                            Chain clone = new Chain(parts);
                            clone.getElements().remove(parts.getElements().size() - 1);
                            String chain = listToString(clone) + part;
                            if (phone.indexOf(chain) == 0) {
                                clone.getElements().add(part);

                            }
                            clonedChains.add(clone);
                        } else {
                            String chain = listToString(parts) + part;
                            if (phone.indexOf(chain) == 0) {
                                parts.getElements().add(part);

                            }
                        }
                    }
                    if (clonedChains != null) {
                        for (Chain clonedChain : clonedChains) {
                            if (!clonedChain.getElements().isEmpty() && phone.indexOf(listToString(clonedChain)) == 0)
                                result.add(clonedChain);
                        }
                    }
                }
            }
        }
        Iterator<Chain> iterator = result.iterator();
        while (iterator.hasNext()) {
            Chain chain = iterator.next();
            if (chain.getElements().isEmpty() || !listToString(chain).equals(phone)) {
                iterator.remove();
            }
        }
        Set<Chain> uniqueResult = Sets.newHashSet(result);
        result = Lists.newArrayList(uniqueResult);
        return result;
    }

    private String listToString(Chain chain) {
        return joinOnEmpty.join(chain.getElements());
    }

    protected Chain findMatchedWords(Map<String, Set<Word>> combinations, List<Chain> chains) {
        Joiner joiner = Joiner.on(" ").skipNulls();
        Chain result = new Chain();

        for (Chain chain : chains) {
            List<Set<String>> collectedWords = Lists.newArrayList();
            for (String element : chain.getElements()) {
                collectedWords.add(Sets.newHashSet(findOriginals(combinations.get(element)).getElements()));
            }

            for (List<String> words : Sets.cartesianProduct(collectedWords)) {
                result.getElements().add(joiner.join(words));
            }
        }

        return result;
    }

    protected List<Chain> filterChains(Map<String, Set<Word>> combinations, List<Chain> chains) {
        List<Chain> filteredChain = Lists.newArrayList();
        for (Chain chain : chains) {
            boolean isContainsCloseNums;
            isContainsCloseNums = checkCloseTwoNumerics(chain, combinations);
            if (!isContainsCloseNums) filteredChain.add(chain);
        }
        return filteredChain;
    }


    private boolean checkCloseTwoNumerics(Chain chain, Map<String, Set<Word>> combinations) {

        for (int i = 0; i < chain.getElements().size() - 1; i++) {
            Set<Word> current = combinations.get(chain.getElements().get(i));
            Set<Word> next = combinations.get(chain.getElements().get(i + 1));
            boolean isCurrentNumber = false;
            boolean isNextNumber = false;
            if (current.size() == 1 && next.size() == 1) {
                for (Word word : current) {
                    if (word.isNumber())
                        isCurrentNumber = true;
                }
                for (Word word : next) {
                    if (word.isNumber())
                        isNextNumber = true;
                }
            }
            if (isCurrentNumber && isNextNumber) return true;
        }
        return false;
    }


    private void addVariants(Map<String, Set<Word>> partVariants, String phonePart, Set<Word> words) {
        if (partVariants.containsKey(phonePart)) {
            Set<Word> values = partVariants.get(phonePart);
            values.addAll(words);
        } else {
            partVariants.put(phonePart, words);
        }
    }

    protected Map<Integer, Chain> findCombinations(String phone) {
        Map<Integer, Chain> indexedCombinations = Maps.newHashMap();
        for (int i = 0; i < phone.length(); i++) {
            Chain chain = new Chain();
            for (int j = i; j < phone.length(); j++) {
                chain.getElements().add(phone.substring(i, j + 1));
            }
            indexedCombinations.put(i, chain);
        }
        return indexedCombinations;
    }

    Chain findOriginals(Set<Word> words) {
        Chain chain = new Chain();
        if (words != null) {
            for (Word word : words) {
                Set<String> originals = word.getOriginals();
                if (originals != null && !originals.isEmpty()) {
                    chain.getElements().addAll(originals);
                }
            }
        }

        return chain;
    }


}
