package com.vostrykov.encoder.dictionary.service;

import java.util.Map;

/**
 * Created by alex on 7/7/15.
 */
public interface IDictionary<K, V> {
    Map<K, V> read();
    boolean insert(String word);
}
