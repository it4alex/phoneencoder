package com.vostrykov.encoder.dictionary.service;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.vostrykov.encoder.dictionary.Validator;
import com.vostrykov.encoder.dictionary.model.Word;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Set;

/**
 * Created by alex on 7/7/15.
 */
public class DictionaryMap implements IDictionary<String, Set<Word>> {
    public static final Logger LOG = Logger.getLogger(DictionaryMap.class);
    private Map<String, Set<Word>> dictionary;
    private String fileName;

    @Inject
    public DictionaryMap(@Named("mainDictionary") String fileName) {
        this.fileName = fileName;
        LOG.info(String.format("Using [%s] dictionary", fileName));
    }

    @Override
    public Map<String, Set<Word>> read() {
        if (dictionary == null) {
            LOG.info("Reading dictionary...");
            dictionary = Maps.newLinkedHashMap();
            long records = 0;
            try {
                try (InputStreamReader is = new InputStreamReader(this.getClass().getResourceAsStream("/" + fileName));
                     BufferedReader br = new BufferedReader(is)) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        if (Validator.isCorrectInputWord(line)) {
                            records++;
                            insert(line);
                        }
                    }
                    LOG.info(String.format("Found [%d] records", records));
                }
            } catch (IOException e) {
                LOG.error(e.getMessage());
                Throwables.propagate(e);
            }
        }
        return dictionary;

    }

    @Override
    public boolean insert(String line) {
        Preconditions.checkArgument(Validator.isCorrectInputWord(line),
                String.format("Line [%s] in dictionary is not correct.", line));

        Word newWord = new Word(line);
        if (dictionary.containsKey(newWord.getNumber())) {
            Set<Word> wordsWithTheSameNumber = dictionary.get(newWord.getNumber());
            if (wordsWithTheSameNumber.contains(newWord)) {
                for (Word word : wordsWithTheSameNumber) {
                    if (word.equals(newWord)) {
                        word.addToOriginals(newWord);
                        return true;
                    }
                }
            } else {
                wordsWithTheSameNumber.add(newWord);
                return true;
            }
        } else {
            Set<Word> newSet = Sets.newLinkedHashSet();
            newSet.add(newWord);
            dictionary.put(newWord.getNumber(), newSet);
            return true;
        }
        return false;
    }
}
