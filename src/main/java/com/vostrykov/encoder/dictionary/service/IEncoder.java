package com.vostrykov.encoder.dictionary.service;

import java.util.List;

/**
 * Created by alex on 7/12/15.
 */
public interface IEncoder<T> {
    List<T> find(T phone);
    List<T> format(T phone, List<T> words);
}
