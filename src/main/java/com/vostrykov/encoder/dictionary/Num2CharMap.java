package com.vostrykov.encoder.dictionary;

import com.google.common.collect.ImmutableMap;

/**
 * Created by alex on 7/7/15.
 */
public class Num2CharMap {
    private static final ImmutableMap<Character, Integer> CHAR_MAP = new ImmutableMap.Builder<Character, Integer>()
            .put('e', 0)
            .put('j', 1)
            .put('n', 1)
            .put('q', 1)
            .put('r', 2)
            .put('w', 2)
            .put('x', 2)
            .put('d', 3)
            .put('s', 3)
            .put('y', 3)
            .put('f', 4)
            .put('t', 4)
            .put('a', 5)
            .put('m', 5)
            .put('c', 6)
            .put('i', 6)
            .put('v', 6)
            .put('b', 7)
            .put('k', 7)
            .put('u', 7)
            .put('l', 8)
            .put('o', 8)
            .put('p', 8)
            .put('g', 9)
            .put('h', 9)
            .put('z', 9)
            .build();

    private Num2CharMap() {
    }

    public static String word2Number(String cleanWord) {
        if (cleanWord == null || cleanWord.isEmpty()) return "";
        StringBuilder result = new StringBuilder(cleanWord.length());
        for (int i = 0; i < cleanWord.length(); i++) {
            Integer num = CHAR_MAP.get(cleanWord.charAt(i));
            if (num != null) result.append(num);
        }
        return result.toString();

    }


}
