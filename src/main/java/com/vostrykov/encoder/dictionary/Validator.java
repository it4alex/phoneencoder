package com.vostrykov.encoder.dictionary;

import com.google.common.base.Preconditions;

/**
 * Created by alex on 7/11/15.
 */
public class Validator {

    public static final int VALID_PHONE_LENGTH = 50;

    private Validator() {
    }

    public static boolean isCorrectInputPhone(String phone) {
        if (phone == null) return false;
        String cleanedPhone = cleanPhone(phone);
        return cleanedPhone.matches("\\d+") && cleanedPhone.length() <= VALID_PHONE_LENGTH;
    }

    public static String cleanPhone(String phone) {
        Preconditions.checkNotNull(phone, "Phone must not be null");
        return phone.replaceAll("-|\\/", "");
    }


    public static boolean isCorrectInputWord(String word) {
        if (word == null) return false;
        String cleanedWord = cleanWord(word);
        return cleanedWord.matches("[a-zA-Z]+");
    }
    public static String cleanWord(String word){
        Preconditions.checkNotNull(word, "Word must not be null");
        return word.replaceAll("-|\"", "");

    }
}
