package com.vostrykov.encoder.dictionary.model;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alex on 7/12/15.
 */
public class Chain {
    List<String> elements;

    public Chain() {
        elements = Lists.newArrayList();
    }

    public Chain(Chain chain) {
        elements = new ArrayList<>(chain.getElements());
    }


    public Chain(String value) {
        this();
        elements.add(value);
    }


    public Chain(List<String> value) {
        this.elements = value;
    }

    public List<String> getElements() {
        return elements;
    }

    public void setElements(List<String> elements) {
        this.elements = elements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Chain)) return false;
        Chain chain = (Chain) o;
        return Objects.equal(getElements(), chain.getElements());
    }

    @Override
    public int hashCode() {
        return getElements().hashCode();
    }

    @Override
    public String toString() {
        Joiner joinOnEmpty = Joiner.on(" ").skipNulls();
        return joinOnEmpty.join(elements);
    }

}
