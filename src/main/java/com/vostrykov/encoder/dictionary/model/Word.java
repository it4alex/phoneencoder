package com.vostrykov.encoder.dictionary.model;

import com.google.common.base.Objects;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.vostrykov.encoder.dictionary.Num2CharMap;
import com.vostrykov.encoder.dictionary.Validator;

import java.util.Set;

/**
 * Created by alex on 7/7/15.
 */
public class Word {
    private final Set<String> originals;
    private final String cleaned;
    private final String number;


    public Word(String word) {
        originals = Sets.newLinkedHashSet();
        this.originals.add(word);
        this.cleaned = Validator.cleanWord(word).toLowerCase();
        this.number = Num2CharMap.word2Number(cleaned);
    }

    public boolean isNumber(){
        return number.isEmpty();
    }

    public String getNumber() {
        return number;
    }

    public String getCleaned() {
        return cleaned;
    }

    public Set<String> getOriginals() {
        return originals;
    }

    public boolean addToOriginals(String word){
        return this.originals.add(word);
    }

    public boolean addToOriginals(Word word){
        return this.originals.addAll(word.getOriginals());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word = (Word) o;
        return Objects.equal(getCleaned(), word.getCleaned());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCleaned());
    }

    @Override
    public String toString() {
        return "{" +
                "originals=" + originals +
                ", cleaned='" + cleaned + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}
