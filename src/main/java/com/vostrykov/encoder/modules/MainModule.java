package com.vostrykov.encoder.modules;

import com.google.inject.*;
import com.google.inject.name.Names;
import com.vostrykov.encoder.Launcher;
import com.vostrykov.encoder.dictionary.model.Word;
import com.vostrykov.encoder.dictionary.service.DictionaryMap;
import com.vostrykov.encoder.dictionary.service.IDictionary;
import com.vostrykov.encoder.dictionary.service.IEncoder;
import com.vostrykov.encoder.dictionary.service.PhoneEncoder;

import java.util.Set;

/**
 * Created by alex on 7/12/15.
 */
public class MainModule implements Module {

    private static Injector injector = null;

    public static Injector getInjector() {
        if (injector == null) {
            injector = Guice.createInjector(new MainModule());
        }
        return injector;
    }

    public static Launcher getEntryPoint() {
        return getInjector().getInstance(Launcher.class);
    }

    @Override
    public void configure(Binder binder) {
        binder.bind(String.class).annotatedWith(Names.named("mainDictionary")).toInstance("dictionary.txt");
        binder.bind(new TypeLiteral<IDictionary<String, Set<Word>>>() {
        }).to(DictionaryMap.class).in(Scopes.SINGLETON);
        binder.bind(new TypeLiteral<IEncoder<String>>() {
        }).to(PhoneEncoder.class).in(Scopes.SINGLETON);
    }
}
