package com.vostrykov.encoder;

import com.google.common.base.Splitter;
import com.google.inject.Inject;
import com.vostrykov.encoder.dictionary.Validator;
import com.vostrykov.encoder.dictionary.service.IEncoder;
import com.vostrykov.encoder.modules.MainModule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Scanner;

/**
 * Created by alex on 7/7/15.
 */
public class Launcher {
    @Inject
    IEncoder<String> encoder;

    private void execute() {
        System.out.println("Plese type 'input' to use internal input file or");
        String message = "enter comma separated numbers or type: \"exit\" to quit.";
        System.out.println(message);
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine().toLowerCase();
            switch (line) {
                case "exit":
                    System.out.println("Bye.");
                    scanner.close();
                    System.exit(0);

                case "input":
                    processInputResourceFile();
                    break;

                default:
                    Iterable<String> parts = Splitter.on(',').trimResults().omitEmptyStrings().split(line);
                    for (String part : parts) {
                        print(part);
                    }
            }
            System.out.println(message);
        }
    }

    private void processInputResourceFile() {
        try {
            try (InputStreamReader is = new InputStreamReader(this.getClass().getResourceAsStream("/" + "input.txt"));
                 BufferedReader bfr = new BufferedReader(is)) {
                String intputLine = null;
                while ((intputLine = bfr.readLine()) != null) {
                    if(Validator.isCorrectInputPhone(intputLine)){
                        print(intputLine);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void print(String phone) {
        List<String> result = encoder.find(phone);
        for (String formatted : encoder.format(phone, result)) {
            System.out.println(formatted);
        }
    }

    public static void main(String[] args) {
        MainModule.getEntryPoint().execute();

    }


}
