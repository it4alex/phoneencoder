package com.vostrykov.encoder.dictionary.model;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 7/12/15.
 */
public class WordTest {
    @Test
    public void testEquals() throws Exception {
        Word w1 = new Word("asdf");
        Word w2 = new Word("as\"d-f");
        assertTrue(w1.equals(w2));
    }

}