package com.vostrykov.encoder.dictionary.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Inject;
import com.vostrykov.encoder.dictionary.model.Chain;
import com.vostrykov.encoder.dictionary.model.Word;
import com.vostrykov.encoder.modules.TestModule;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by alex on 7/12/15.
 */
public class PhoneEncoderTest {
    @Inject
    IEncoder<String> encoder;
    PhoneEncoder phoneEncoder;

    public PhoneEncoderTest() {
        TestModule.getInjector().injectMembers(this);
        phoneEncoder = new PhoneEncoder();
    }

    @Test
    public void testFind() throws Exception {
        List<String> list56482 = encoder.find("5624-82");
        assertEquals(2, list56482.size());
        assertTrue(list56482.contains("mir Tor"));
        assertTrue(list56482.contains("Mix Tor"));

        List<String> list4824 = encoder.find("4824");
        assertEquals(3, list4824.size());
        assertTrue(list4824.contains("Torf"));
        assertTrue(list4824.contains("fort"));
        assertTrue(list4824.contains("Tor 4"));

        List<String> list04824 = encoder.find("04824");
        assertEquals(3, list4824.size());
        assertTrue(list04824.contains("0 Torf"));
        assertTrue(list04824.contains("0 fort"));
        assertTrue(list04824.contains("0 Tor 4"));

        List<String> list381482 = encoder.find("381482");
        assertEquals(1, list381482.size());
        assertEquals("so 1 Tor", list381482.get(0));

        List<String> list107835 = encoder.find("10/783--5");
        assertEquals(3, list107835.size());
        assertTrue(list107835.contains("je Bo\" da"));
        assertTrue(list107835.contains("je bo\"s 5"));
        assertTrue(list107835.contains("neu o\"d 5"));

        List<String> listOneNumber = encoder.find("9");
        assertEquals(0, listOneNumber.size());
    }

    @Test
    public void testFormat() throws Exception {
        String phone = "10/783--5";
        List<String> matchedWords = Lists.newArrayList("je Bo\" da", "je bo\"s 5", "neu o\"d 5");
        List<String> formattedList = encoder.format(phone, matchedWords);

        assertEquals(3, formattedList.size());
        assertTrue(formattedList.contains("10/783--5: je Bo\" da"));
        assertTrue(formattedList.contains("10/783--5: je bo\"s 5"));
        assertTrue(formattedList.contains("10/783--5: neu o\"d 5"));
    }


    @Test
    public void testIsContainsWordAnalog() throws Exception {

        Chain notContains = new Chain(Lists.newArrayList("ab bc cd", "ab bc c 7", "abbc 7 c"));
        Chain containsFirst = new Chain(Lists.newArrayList("ab bc cd", "1 b bc cd", "ab bc7 c"));
        Chain containsMiddle = new Chain(Lists.newArrayList("ab bc cd", "ab 7 c cd", "ab bc7 c"));
        assertFalse(phoneEncoder.isContainsWordAnalog("ab bc c 7", notContains));
        assertTrue(phoneEncoder.isContainsWordAnalog("1 b bc cd", containsFirst));
        assertTrue(phoneEncoder.isContainsWordAnalog("ab 7 c cd", containsMiddle));

        final Chain chain = new Chain(Lists.newArrayList("je Bo\" da", "je bo\"s 5", "neu o\"d 5", "neu 8 da", "je 7 o\"d 5"));
        assertFalse(phoneEncoder.isContainsWordAnalog("je Bo\" da", chain));
        assertFalse(phoneEncoder.isContainsWordAnalog("je bo\"s 5", chain));
        assertFalse(phoneEncoder.isContainsWordAnalog("neu o\"d 5", chain));
        assertTrue(phoneEncoder.isContainsWordAnalog("neu 8 da", chain));
        assertTrue(phoneEncoder.isContainsWordAnalog("je 7 o\"d 5", chain));

    }

    @Test
    public void testFindCombinations() throws Exception {
        Map<Integer, Chain> combinations = phoneEncoder.findCombinations("1234");
        assertEquals(4, combinations.size());
        assertEquals("1 12 123 1234", combinations.get(0).toString());
        assertEquals("2 23 234", combinations.get(1).toString());
        assertEquals("3 34", combinations.get(2).toString());
        assertEquals("4", combinations.get(3).toString());
    }

    @Test
    public void testFindOriginals() throws Exception {
        Word word1 = new Word("YYY");
        Word word2 = new Word("XXX");
        word2.addToOriginals("xxx");
        Set<Word> words = Sets.newLinkedHashSet();
        words.add(word1);
        words.add(word2);
        assertEquals("YYY XXX xxx", phoneEncoder.findOriginals(words).toString());
    }
}