package com.vostrykov.encoder.dictionary.service;

import com.google.inject.Inject;
import com.vostrykov.encoder.dictionary.model.Word;
import com.vostrykov.encoder.modules.TestModule;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

/**
 * Created by alex on 7/12/15.
 */
public class DictionaryMapTest {
    @Inject
    IDictionary<String, Set<Word>> dictionary;

    public DictionaryMapTest() {
        TestModule.getInjector().injectMembers(this);
    }

    @Test
    public void testRead() throws Exception {
        assertEquals(21, dictionary.read().size());

    }

    @Test
    public void testInsert() throws Exception {
        assertEquals(21, dictionary.read().size());
        assertTrue(dictionary.insert("xxxxx"));
        assertTrue(dictionary.insert("YYYYY"));
        assertEquals(23, dictionary.read().size());
    }
}