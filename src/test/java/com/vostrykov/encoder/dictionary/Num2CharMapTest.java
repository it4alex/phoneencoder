package com.vostrykov.encoder.dictionary;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 7/9/15.
 */
public class Num2CharMapTest {

    @Test
    public void testWord2Number() throws Exception {
        assertEquals("2823",Num2CharMap.word2Number("word"));
        assertEquals("",Num2CharMap.word2Number(""));
        assertEquals("562482", Num2CharMap.word2Number("mixtor"));
        assertEquals("562482", Num2CharMap.word2Number("mirtor"));

    }
}