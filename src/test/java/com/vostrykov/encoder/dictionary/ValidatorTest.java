package com.vostrykov.encoder.dictionary;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by alex on 7/12/15.
 */
public class ValidatorTest {


    /***
     * Телефнный номер может состоять из следующих символов идущих в любом порядке: - (dash), / (slash), цифры.
     Символы - и / при кодировании должны игнорироваться. Максимальная длинна телефонного номера - 50 символов.
     * @throws Exception
     */
    @Test
    public void testIsCorrectInputPhone() throws Exception {
        assertTrue(Validator.isCorrectInputPhone("564"));
        assertTrue(Validator.isCorrectInputPhone("56-4"));
        assertTrue(Validator.isCorrectInputPhone("5/6/4"));

        assertFalse(Validator.isCorrectInputPhone(""));
        assertFalse(Validator.isCorrectInputPhone("-"));
        assertFalse(Validator.isCorrectInputPhone(null));
        assertFalse(Validator.isCorrectInputPhone("423s"));
        assertFalse(Validator.isCorrectInputPhone("4,23"));
        assertFalse(Validator.isCorrectInputPhone("4&23"));
        assertFalse(Validator.isCorrectInputPhone("4 23"));
        assertFalse(Validator.isCorrectInputPhone("   "));

    }


    @Test
    public void testCleanPhone() throws Exception {
        assertEquals("564",Validator.cleanPhone("56-4"));
        assertEquals("564", Validator.cleanPhone("5/6/4"));
    }



    /**
     * Слова в словаре могут содержать заглавные буквы, дефисы (-) и двойные кавычки (").
     * @throws Exception
     */
    @Test
    public void testIsCorrectInputWord() throws Exception {
        assertTrue(Validator.isCorrectInputWord("word"));
        assertTrue(Validator.isCorrectInputWord("w-ord"));
        assertTrue(Validator.isCorrectInputWord("w\"ord"));
        assertTrue(Validator.isCorrectInputWord("WORD"));

        assertFalse(Validator.isCorrectInputWord("w   ord"));
        assertFalse(Validator.isCorrectInputWord("w.ord"));
        assertFalse(Validator.isCorrectInputWord("w\'ord"));
        assertFalse(Validator.isCorrectInputWord("--"));
        assertFalse(Validator.isCorrectInputWord("w//ord"));
        assertFalse(Validator.isCorrectInputWord("w22ord"));
        assertFalse(Validator.isCorrectInputWord("  "));
        assertFalse(Validator.isCorrectInputWord(null));

    }

    @Test
    public void testCleanWord() throws Exception {
        assertEquals("word", Validator.cleanWord("w-ord"));
        assertEquals("word", Validator.cleanWord("w\"ord"));
    }
}